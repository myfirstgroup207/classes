using System;
using System.Collections.Generic;
using System.Linq;

namespace Class
{
    //TODO: Create public class Rectangle here

    public class Rectangle
    {
        //TODO: Define two private double fields: 'sideA', 'sideB'

        private double sideA, sideB;

        //TODO: Define constructor with two double parameters: 'a', 'b'. Assign parameters to sides
        public Rectangle(double a, double b)
        {
            sideA= a;
            sideB= b;
        }
        //TODO: Define constructor with double parameter: 'a'. Parameter sets side A of rectangle, and B-side always equals 5
        public Rectangle(double a)
        {
            sideA = a;
            sideB = 5;
        }

        //TODO: Define constructor without parameters. A-side of rectangle equals 4, B-side - 3
        public Rectangle()
        {
            this.sideA = 4;
            this.sideB = 3;
        }
        //TODO: Define public method 'GetSideA' that returns value of A-side
        public double GetSideA()
        {
            return sideA;
        }
        //TODO: Define public method 'GetSideB' that returns value of B-side
        public double GetSideB()
        {
            return sideB;
        }
        //TODO: Define public method 'Area' that calculates and returns value of area
        public double Area()
        {
            return sideA * sideB;
        }
        //TODO: Define public method 'Perimeter' that calculates and returns value of perimeter
        public double Perimeter()
        {
            return 2*(sideA + sideB);
        }
        //TODO: Define public method 'IsSquare' that checks if rectangle is a square. Returns true if it is a square, false if it's not.
        public bool IsSquare()
        {
            return sideA == sideB;
        }
        //TODO: Define public method 'ReplaceSides' that swaps values of rectangle sides
        public void ReplaceSides()
        {
            double temp = sideA;
            sideA = sideB;
            sideB = temp;
        }


    }
    //TODO: Create public class ArrayRectangles here
    public class ArrayRectangles
    {
        //TODO: Define private field that is array of rectangles: rectangle_array
        private Rectangle[] rectangle_array;

        //TODO: Define constructor with int parameter: 'n'. Constructor should create an empty array of rectangles with length of 'n'
        public ArrayRectangles(int n)
        {
            rectangle_array = new Rectangle[n];
        }
        //TODO: Define public method 'AddRectangle' that adds rectangle on the first empty place of array field. Returns true if array has empty space, if not
        public bool AddRectangle(Rectangle rectangle)
        {
            for(int i=0;i<rectangle_array.Length;i++)
            {
                if (rectangle_array[i] == null)
                {
                    rectangle_array[i] = rectangle;
                    return true;
                }
            }
            return false;
        }
        //TODO: Define public method 'NumberMaxArea' that returns number of rectangle with max value of area. Numbering starts from 0
        public int NumberMaxArea()
        {
            double max = rectangle_array[0].Area();
            int ind = 0;
            for(int i = 1; i < rectangle_array.Length; i++)
            {
                if (rectangle_array[i].Area() > max)
                {
                    max = rectangle_array[i].Area();
                    ind = i;
                }
            }
            return ind;
        }
        //TODO: Define public method 'NumberMinPerimeter' that returns number of rectangle with min value of perimeter. Numbering starts from 0
        public int NumberMinPerimeter()
        {
            double min = rectangle_array[0].Area();
            int ind = 0;
            for (int i = 1; i < rectangle_array.Length; i++)
            {
                if (rectangle_array[i].Area() < min)
                {
                    min = rectangle_array[i].Area();
                    ind = i;
                }
            }
            return ind;
        }
        //TODO: Define public method 'NumberSquare' that returns amount of squares in array of rectangles
        public int NumberSquare()
        {
            int count = 0;
            for(int i = 0; i < rectangle_array.Length; i++)
            {
                if (rectangle_array[i].IsSquare())
                {
                    count++;
                }
            }
            return count;
        }
    }
}
